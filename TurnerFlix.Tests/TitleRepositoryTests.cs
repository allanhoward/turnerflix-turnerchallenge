﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using TurnerFlix.Data;
using TurnerFlix.Repositories;

namespace TurnerFlix.Tests
{
    [TestClass]
    public class TitleRepositoryTests
    {
        [TestMethod]
        [TestCategory("Integration")]
        public void ListTitles_WithValidInput_ReturnsTitles()
        {
            //Arrange
            var repository = new TitleRepository(new TitlesEntities());

            //Act
            var titles = repository.ListTitles("Br",10);

            //Assert
            Assert.IsNotNull(titles);
            Assert.IsTrue(titles.Any());
        }
        
        [TestMethod]
        [TestCategory("Integration")]
        public void GetTitleById_WithValidId_ReturnsTitle()
        {
            //Arrange
            var repository = new TitleRepository(new TitlesEntities());

            //Act
            var title = repository.GetTitleById(69593);

            //Assert
            Assert.IsNotNull(title);
        }

        [TestMethod]
        public void ListTitles_WithSearchKey_ReturnsSingleTitle()
        {
            var data = new List<Title> 
            { 
                new Title { TitleId=1, TitleName = "BBB", TitleNameSortable = "BBB" }, 
                new Title { TitleId=2, TitleName = "ZZZ", TitleNameSortable = "ZZZ" }, 
                new Title { TitleId=3, TitleName = "AAA", TitleNameSortable = "AAA" }, 
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Title>>();
            mockSet.As<IQueryable<Title>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Title>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Title>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Title>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<TitlesEntities>();
            mockContext.Setup(c => c.Titles).Returns(mockSet.Object);

            var repository = new TitleRepository(mockContext.Object);
            var titles = repository.ListTitles("B",10);

            Assert.AreEqual(1, titles.Count());
            Assert.AreEqual("BBB", titles.First().TitleName);           
        }
    }
}
