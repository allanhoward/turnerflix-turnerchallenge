﻿using System.Web.Mvc;
using TurnerFlix.Data;
using TurnerFlix.Repositories;

namespace TurnerFlix.Controllers
{
    public class TitleController : Controller
    {
        private readonly ITitleRepository _repository;

        public TitleController()
        {
            _repository = new TitleRepository(new TitlesEntities());
        }

        public TitleController(ITitleRepository repository)
        {
            _repository = repository;
        }

        public ActionResult SearchTitles(string searchKey, int numberOfRecords)
        {
            var titles = _repository.ListTitles(searchKey, numberOfRecords);
            return Json(titles);
        }
        
        public ActionResult GetTitle(int id)
        {
            var title = _repository.GetTitleById(id);
            return Json(title);
        }
    }
}