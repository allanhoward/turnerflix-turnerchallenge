﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TurnerFlix.Data;
using TurnerFlix.Repositories;

namespace TurnerFlix.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        
        public ActionResult About()
        {
            ViewBag.Message = "This Turner challenge application makes use of jQuery, Bootstrap3, C#, MSTest, Moq, Asp.Net MVC, and Entity Framework.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "This Turner challenge was submitted by ";

            return View();
        }
    }
}