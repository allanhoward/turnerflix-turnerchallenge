﻿namespace TurnerFlix.Models
{
    public class TitleViewModel
    {
        public int TitleId { get; set; }
        public string TitleName { get; set; }
        public string Summary { get; set; }
        public int Year { get; set; }
        public string Genre { get; set; }
        public string Awards { get; set; }
        public string Cast { get; set; }
        public string Director { get; set; }
    }
}