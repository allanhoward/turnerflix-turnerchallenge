﻿using System.Collections.Generic;
using TurnerFlix.Models;

namespace TurnerFlix.Repositories
{
    public interface ITitleRepository
    {
        List<TitleViewModel> ListTitles(string searchKey, int numberOfResults);

        TitleViewModel GetTitleById(int id);       
    }
}
