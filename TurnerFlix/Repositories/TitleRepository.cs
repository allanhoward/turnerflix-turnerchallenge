﻿using System;
using System.Collections.Generic;
using System.Linq;
using TurnerFlix.Data;
using TurnerFlix.Models;

namespace TurnerFlix.Repositories
{
    public class TitleRepository : ITitleRepository, IDisposable
    {
        private readonly TitlesEntities _dbContext;

        public TitleRepository(TitlesEntities dbContext)
        {
            _dbContext = dbContext;
        }
        
        public List<TitleViewModel> ListTitles(string searchKey, int numberOfResults)
        {
            var query = (from t in _dbContext.Titles
                where t.TitleNameSortable.Contains(searchKey)
                select new TitleViewModel
                {
                    TitleId = t.TitleId,
                    TitleName = t.TitleName,
                    Year = t.ReleaseYear ?? 0
                }).Take(numberOfResults);
            return query.ToList();
        }

        public TitleViewModel GetTitleById(int id)
        {
            TitleViewModel model = null;
            var title = _dbContext.Titles.Find(id);   
            if (title != null)
            {
                model = new TitleViewModel
                {
                    TitleId = title.TitleId,
                    TitleName = title.TitleName,
                    Year = title.ReleaseYear ?? 0,
                    Genre = title.TitleGenres.First().Genre.Name,
                    Summary = title.StoryLines.First().Description,
                    Awards = GetAward(title),
                    Cast = GetCast(title),
                    Director = GetDirector(title)
                };
            }
            return model;
        }

        private string GetAward(Title title)
        {
            var award = title.Awards
                .Where(a => a.AwardWon.HasValue && a.AwardWon.Value)
                .OrderByDescending(a => a.AwardYear)
                .FirstOrDefault();
            return award!=null ? award.Award1 : string.Empty;
        }

        private string GetCast(Title title)
        {
            var actors = (from p in _dbContext.Participants
                          join tp in _dbContext.TitleParticipants
                          on new { Id = p.Id, TitleId = title.TitleId } equals
                          new { Id = tp.ParticipantId, TitleId = tp.TitleId }
                          where tp.IsKey && tp.IsOnScreen && tp.RoleType == "Actor"
                          select p.Name).Take(2);

            string cast = string.Empty;
            if (actors.Any())
            {
                cast = string.Join(", ", actors);
            }
            return cast;
        }

        private string GetDirector(Title title)
        {
            var director = (from p in _dbContext.Participants
                        join tp in _dbContext.TitleParticipants 
                        on new {Id = p.Id, TitleId = title.TitleId} equals
                        new {Id = tp.ParticipantId, TitleId = tp.TitleId}
                        where tp.IsKey && tp.RoleType == "Director"
                        select p.Name).FirstOrDefault();
              
            return director ?? string.Empty;
        }


        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}