﻿$(document).ready(function() {
    $("#searchForm").submit(function (e) {
        e.preventDefault();
        searchTitles($("#searchKey").val(), 100);
    });

    $("#searchKey").keyup(function () {
        searchTitles($("#searchKey").val(), 100);
    });

    $("#rowContainer").on("click", ".detailLink", function () {        
        getTitleDetail($(this).attr("rel"));
    });

    $(document).on({
        ajaxStart: function () { $("body").addClass("loading"); },
        ajaxStop: function () { $("body").removeClass("loading"); }
    });
});

function searchTitles(key,records) {
    if (key.length < 2) return;

    $.post("/Title/SearchTitles", { searchKey: key, numberOfRecords: records },
        function (data) { writeTitle(data);
      });    
}

function writeTitle(titles) {
    $("#rowContainer").empty();
    var rowContent = '';
    for (var i = 0; i < titles.length; i++) {
        var title = titles[i];
        if (i % 5 == 0) {
            if (i > 0) {
                appendRowContent(rowContent);
                rowContent = '';
            }
                        
            rowContent += '<div class="col-md-offset-1 col-md-2">';
            rowContent += getRowContent(title);
        } else {
            rowContent += '<div class="col-md-2">';
            rowContent += getRowContent(title);
        }                
    }

    if (titles.length == 0) {
        rowContent = 'No matches found.  Search again.';
    }

    appendRowContent(rowContent);
    wirePopup();
}

function appendRowContent(rowContent)
{
    rowContent = '<div class="row row-pad">' + rowContent + '</div>';
    $("#rowContainer").append(rowContent);    
}

function getRowContent(title) {
    return '<div class="title-border"><h4>' + title.TitleName + '</h4>' +
        'Year:  <span class="year">' + title.Year + '</span><br/>' +
        '<p><a class="detailLink" href="javascript:void(0);" id="detailLink'+title.TitleId+'" rel="' + title.TitleId + '">more &raquo;</a></p>' +
        '</div></div>';
}

function getTitleDetail(titleId) {
    if (titleId<0) return;

    $.post("/Title/GetTitle", { id: titleId},
        function (data) {
            writeTitleDetail(data, titleId);
        });
}

function writeTitleDetail(title, id) {
    $("#titleDetail .genre").html(title.Genre);   
    $("#titleDetail .summary").html(title.Summary);
    $("#titleDetail .cast").html(title.Cast);
    $("#titleDetail .director").html(title.Director);
    $("#titleDetail .award").html(title.Awards);
    $("#titleDetail .year").html(title.Year);
    $("#titleDetail h2").html(title.TitleName);
    $("#detailLink"+id).btOn();
}

function wirePopup() {
    $(".detailLink").bt({
        trigger:'none',
        contentSelector: "$('#titleDetail')",
        centerPointY: .1,
        positions: ['right'],
        padding: 0,
        width: 256,
        spikeGirth: 60,
        spikeLength: 50,
        cornerRadius: 10,
        fill: '#FFF',
        strokeStyle: '#B9090B',
        shadow: true,
        shadowBlur: 12,
        shadowOffsetX: 0,
        shadowOffsetY: 5,
        hoverIntentOpts: { interval: 300, timeout: 0 },
        cssStyles: {
            fontSize: '12px',
            fontFamily: 'arial,helvetica,sans-serif'
        }
    });
}